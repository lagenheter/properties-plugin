<?php
/**
 * Plugin Name:     jimmitjoo Properties
 * Plugin URI:      https://lagenheter24.se
 * Description:     A plugin to display properties from our own service.
 * Author:          Jimmie Johansson
 * Author URI:      https://lagenheter24.se
 * Text Domain:     properties
 * Domain Path:     /languages
 * Version:         1.0.0
 *
 * @package         Properties
 */

// Your code starts here.

function get_api_root(): string
{
	$rootApiUri = env('PLOMMON_PROPERTIES_URL');

	return $rootApiUri;
}

function fetch_properties_by_coordinates($atts = [])
{
	$cache_key = 'latitude=' . $atts['latitude'] . '&longitude=' . $atts['longitude'];

	$response = get_transient($cache_key);
	if ($response === false) {
		$args = array(
			'method' => 'GET'
		);
		$response = wp_remote_request(get_api_root() . '/geolist?latitude=' . $atts['latitude'] . '&longitude=' . $atts['longitude'], $args);

		set_transient($cache_key, $response, 240);
	}
	$data = json_decode($response['body']);

	$html = '';
	$i = 0;
	$limit = 10;
	foreach ($data as $property) {
		if (isset($atts['students'])) {
			if ($property->students) {
				$html .= property_listing($property);
				$i++;
			}
		} else {
			$html .= property_listing($property);
			$i++;
		}

		if ($i >= 10) break;
	}


	return $html;
}

function fetch_property_listings($atts = [])
{
	if (!isset($atts['page'])) {
		$atts['page'] = 1;
	}

	if (!isset($atts['limit'])) {
		$atts['limit'] = 10;
	}

	if (isset($atts['town'])) {
		$cache_key = 'limit=' . $atts['limit'] . '&town=' . $atts['town'] . '&page=' . $atts['page'];
	} else {
		$cache_key = 'limit=' . $atts['limit'] . '&page=' . $atts['page'];
	}

	$response = get_transient($cache_key);
	if ($response === false) {
		$args = array(
			'method' => 'GET'
		);
		if (isset($atts['town'])) {
			$response = wp_remote_request(get_api_root() . '/list?limit=' . $atts['limit'] . '&town=' . $atts['town'] . '&page=' . $atts['page'], $args);
		} else {
			$response = wp_remote_request(get_api_root() . '/list?limit=' . $atts['limit'] . '&page=' . $atts['page'], $args);
		}

		if (is_array($response)) {
			set_transient($cache_key, $response, 240);
		}
	}
	$data = json_decode($response['body']);

	$html = '';
	foreach ($data->properties as $property) {
		if (!isset($atts['town'])) {
			$html .= property_listing($property, true);
		} else {
			$html .= property_listing($property);
		}
	}


	return $html;
}

function property_listing($data, $show_town = false)
{

	$html = '<article class="' . strtolower($data->type) . '">';

	$html .= '<div class="article-row">';
	$html .= '<ul>';
	if ($data->type == 'Room') {
		$html .= ' <li>Rum</li>';
	}

	if ($data->students) {
		$html .= '<li>Studentlägenhet</li>';
	}
	if ($data->youths) {
		$html .= '<li>Ungdomslägenhet</li>';
	}
	if ($data->seniors) {
		$html .= '<li>Seniorbostad</li>';
	}

	if ($data->short_term) {
		$html .= '<li>Korttidskontrakt</li>';
	}
	if ($data->newly_constructed) {
		$html .= '<li>Nyproduktion</li>';
	}

	if ($data->furnished) {
		$html .= '<li>Möblerad</li>';
	}
	if ($data->shared) {
		$html .= '<li>Delad bostad</li>';
	}
	$html .= '</ul>';
	$html .= '</div>';

	$html .= '<div class="article-row">';
	if (!$show_town) {
		$html .= '<div><a target="_blank" href="' . $data->external_url . '"><strong> ' . $data->address . '</strong></a></div>';
	} else {
		$html .= '<div><a target="_blank" href="' . $data->external_url . '"><strong> ' . $data->address . ', ' . $data->town . '</strong></a></div>';
	}
	$html .= '<ul>';
	if ($data->rooms > 0) {
		$html .= '<li>' . $data->rooms . ' rum</li>';
	}
	if ($data->area > 0) {
		$html .= '<li>' . $data->area . ' m <sup>2</sup></li>';
	}
	if ($data->rent > 0) {
		$html .= '<li> ' . number_format($data->rent, 0, ',', ' ') . ' kr / mån</li>';
	}
	$html .= '</ul>';

	$html .= '<ul>';
	if ($data->deposition > 0) {
		$html .= '<li> Deposition: ' . number_format($data->deposition, 0, ',', ' ') . ' kr </li>';
	}
	if (strtotime($data->move_in_date)) {
		$html .= '<li> Inflytt: ' . $data->move_in_date . ' </li>';
	}
	if (strtotime($data->move_out_date)) {
		$html .= '<li> Utflytt: ' . $data->move_out_date . ' </li>';
	}
	if (strtotime($data->apply_before_date)) {
		$html .= '<li> Ansök senast: ' . $data->apply_before_date . ' </li>';
	}
	$html .= '</ul>';

	$html .= '<em class="source"> Källa: ' . $data->source . ' </em>';
	$html .= '</div>';

	$html .= '</article>';

	return $html;
}

add_shortcode('property_listing', 'fetch_property_listings');
add_shortcode('property_listing_by_coordinates', 'fetch_properties_by_coordinates');
